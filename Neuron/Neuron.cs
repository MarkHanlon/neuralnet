using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace NeuralNet
{

    public interface INeuronInput : INotifyPropertyChanged
    {
         
        Neuron Owner { get;}
    }

    public abstract class NeuronInput : INeuronInput
    {
        protected Neuron _owner;
        protected float _inputValue;
        protected float _weight;

        public NeuronInput(Neuron owner, float weight, float inputValue)
        {
            _owner = owner;
            _inputValue = inputValue;
            _weight = weight;         
        }
        public float InputValue 
        { 
            get 
            { 
                return _inputValue;
            }
            
            set 
            { 
                if (_inputValue != value)
                {
                    _inputValue = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("InputValue"));
                }
            }
        }

        public float Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        public Neuron Owner
        {
            get { return _owner; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public abstract class Neuron : INotifyPropertyChanged
    {
        protected List<INeuronInput>   _inputs;        
        protected float                   _bias;
        protected float                   _output;

        public event PropertyChangedEventHandler PropertyChanged;

        public Neuron(float bias)
        {
            _inputs = new List<INeuronInput>();            
            _bias = bias;
            _output = 0;
        }

        public INeuronInput AttachInput(INeuronInput nIn)
        {
            if (nIn.Owner != this)
            {
                throw new Exception("Input is not owned by this Neuron");
            }

            _inputs.Add(nIn);

            // Register for Input change events so that the output can be updated
            nIn.PropertyChanged += new PropertyChangedEventHandler(HandleInputChanged);

            Run(); // Do this to update the output now
            return nIn;
        }

        private void HandleInputChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "InputValue")
            {
                if (sender is NeuronInput pIn)
                {
                    pIn.Owner.Run();
                }
            }
        }

        public float Run()
        {
            float oldOutput = _output;
            _output = ActivationFunction();

            if (oldOutput != _output)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("OutputValue"));
            }

            return _output;
        }

        protected abstract float ActivationFunction();       
        
        public float OutputValue
        {
            get { return _output; }
            set { _output = value; }
        }
        
    }

}
