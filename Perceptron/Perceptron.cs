﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace NeuralNet
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }

    public class PerceptronInput : NeuronInput, INeuronInput
    {
        public PerceptronInput(Perceptron owner, float weight, float inputValue) 
        : base(owner, weight, inputValue)
        {            
        }
        
    }

    public class Perceptron : Neuron, INotifyPropertyChanged
    {
        public Perceptron(float bias)
        : base(bias)
        {
        }

         private void HandleInputChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "InputValue")
            {
                if (sender is PerceptronInput pIn)
                {
                    pIn.Owner.Run();
                }
            }
        }

        protected override float ActivationFunction()
        {
            float runningSum = 0;
            foreach (PerceptronInput i in _inputs)
            {
                runningSum += i.InputValue * i.Weight;
                //index++;
            }

            float output;
            if (runningSum + _bias > 0f)
                output = 1;
            else
                output = 0;

            return output;
        }
        
    }

}
