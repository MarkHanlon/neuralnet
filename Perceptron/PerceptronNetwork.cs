using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace NeuralNet
{
    public class PerceptronNetwork
    {
        private Dictionary<Perceptron, List<PerceptronInput>> _networkDict = new Dictionary<Perceptron, List<PerceptronInput>>();

        public void JoinOutputToInput(Perceptron output, PerceptronInput input)
        {
            if (_networkDict.ContainsKey(output))
            {
                if (_networkDict[output].Contains(input))
                    return; // already connected

                _networkDict[output].Add(input);

            }
            else
            {
                _networkDict[output] = new List<PerceptronInput>{input};
                output.PropertyChanged += new PropertyChangedEventHandler(HandlePerceptronChanged);                
            }

            // Now they are joined, ensure the current output value updates the input value
            input.InputValue = output.OutputValue;
        }

        private void HandlePerceptronChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "OutputValue")
            {
                // Output value changed - does it connect to any inputs?
                if (sender is Perceptron p)
                {
                    if (_networkDict.ContainsKey(p))
                    {
                        foreach (PerceptronInput pIn in _networkDict[p])
                        {
                            pIn.InputValue = p.OutputValue;
                        }
                    }
                }
            }
        }
    }
}