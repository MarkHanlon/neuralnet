using System;
using Xunit;
using NeuralNet;

namespace NeuralNet.Tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(0,0,1)]
        [InlineData(0,1,1)]
        [InlineData(1,0,1)]
        [InlineData(1,1,0)]
        public void Perceptron_Acts_Like_Nand_Gate(int in1, int in2, int expected)
        {
            Perceptron p = new Perceptron(bias: 3);
            PerceptronInput pIn1 = new PerceptronInput(p, -2, in1);
            PerceptronInput pIn2 = new PerceptronInput(p, -2, in2);
            p.AttachInput(pIn1);
            p.AttachInput(pIn2);
            float out1 = p.Run();

            Assert.Equal(expected, out1);
        }

        [Theory]
        [InlineData(0,0,0)]
        [InlineData(0,1,0)]
        [InlineData(1,0,0)]
        [InlineData(1,1,1)]
        public void Two_Linked_Perceptrons_Act_Like_And_Gate(int in1, int in2, int expected)
        {
            Perceptron p1 = new Perceptron(bias: 3);
            p1.AttachInput(new PerceptronInput(p1, -2, in1));
            p1.AttachInput(new PerceptronInput(p1, -2, in2));

            Perceptron p2 = new Perceptron(bias: 3);            
            PerceptronInput p2In1 = new PerceptronInput(p2, -2, 0);
            PerceptronInput p2In2 = new PerceptronInput(p2, -2, 0);
            p2.AttachInput(p2In1);
            p2.AttachInput(p2In2);

            PerceptronNetwork network = new PerceptronNetwork();
            network.JoinOutputToInput(p1, p2In1);
            network.JoinOutputToInput(p1, p2In2);

            p1.Run();
            Assert.Equal(expected, p2.OutputValue);
        }

        
        [Theory]
        [InlineData(0,0,0,0)]
        [InlineData(0,1,1,0)]
        [InlineData(1,0,1,0)]
        [InlineData(1,1,0,1)]
        public void Adder_Made_From_Perceptrons(int in1, int in2, int expectedSum, int expectedCarry)
        {
            // in1---|----|
            //       | p1 |---p1
            // in2---|----|

            // in1---|----|
            //       | p2 |---p2
            //  p1---|----|

            //  p1---|----|
            //       | p3 |---p3
            // in2---|----|

            //  p2---|----|
            //       | p4 |---p4
            //  p3---|----|

            //  p1---|----|
            //       | p5 |---p5
            //  p1---|----|

            Perceptron p1 = new Perceptron(bias: 3);
            p1.AttachInput(new PerceptronInput(p1, -2, in1));
            p1.AttachInput(new PerceptronInput(p1, -2, in2));

            Perceptron p2 = new Perceptron(bias: 3);
            PerceptronInput p2In1 = (PerceptronInput)p2.AttachInput(new PerceptronInput(p2, -2, in1));
            PerceptronInput p2In2 = (PerceptronInput)p2.AttachInput(new PerceptronInput(p2, -2, 0));

            Perceptron p3 = new Perceptron(bias: 3);
            PerceptronInput p3In1 = (PerceptronInput)p3.AttachInput(new PerceptronInput(p3, -2, 0));
            PerceptronInput p3In2 = (PerceptronInput)p3.AttachInput(new PerceptronInput(p3, -2, in2));

            Perceptron p4 = new Perceptron(bias: 3);
            PerceptronInput p4In1 = (PerceptronInput)p4.AttachInput(new PerceptronInput(p4, -2, 0));
            PerceptronInput p4In2 = (PerceptronInput)p4.AttachInput(new PerceptronInput(p4, -2, 0));

            Perceptron p5 = new Perceptron(bias: 3);
            PerceptronInput p5In1 = (PerceptronInput)p5.AttachInput(new PerceptronInput(p5, -2, 0));
            PerceptronInput p5In2 = (PerceptronInput)p5.AttachInput(new PerceptronInput(p5, -2, 0));

            PerceptronNetwork network = new PerceptronNetwork();
            network.JoinOutputToInput(p1, p2In2);
            network.JoinOutputToInput(p1, p3In1);
            network.JoinOutputToInput(p2, p4In1);
            network.JoinOutputToInput(p3, p4In2);
            network.JoinOutputToInput(p1, p5In1);
            network.JoinOutputToInput(p1, p5In2);

            p1.Run();
            Assert.Equal(expectedSum, p4.OutputValue);
            Assert.Equal(expectedCarry, p5.OutputValue);
        }
    }
}
